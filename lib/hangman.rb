require 'byebug'

class Hangman
  attr_reader :guesser, :referee, :board, :misses

  def initialize(options = {})
    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    guesser.register_secret_length(secret_word_length)
    @board = Array.new(secret_word_length)
  end

  def take_turn
    player_guess = guesser.guess(board)
    hits = referee.check_guess(player_guess)
    self.update_board(player_guess, hits)

    system('clear')
    guesser.handle_response(player_guess, hits)
  end

  def update_board(guess, hits)
    @misses += 1 if hits.empty?
    hits.each { |hit| @board[hit] = guess }
  end

  def play
    self.setup
    @misses = 0
    while board.include?(nil) && misses < 8
      self.take_turn
      self.render
    end

    puts 'Guesser wins!' if misses != 8
    puts "Guesser loses, word is #{referee.secret_word}" if misses == 8
  end

  def render
    render_hangman(@misses)
    print board.map { |letter| letter.nil? ? '_' : letter }.join + "\n"
  end

  def render_hangman(misses)
    map = []
    7.times { map.push(Array.new(5)) }
    map[0][0], map[0][1], map[0][2] = ['-', '-', '|']
    map[1][0], map[2][0], map[3][0] = ['|', '|', '|']
    map[4][0], map[5][0], map[6][0] = ['|', '|', '|']
    map[1][2] = 'O' if misses > 0
    map[2][2], map[3][2] = ['|', '|'] if misses > 1
    map[4][1], map[5][1] = ['|', '|'] if misses > 2
    map[4][3], map[5][3] = ['|', '|'] if misses > 3
    map[2][1], map[3][1] = ['|', '|'] if misses > 4
    map[2][3], map[3][3] = ['|', '|'] if misses > 5
    map[4][1], map[4][3] = ['o', 'o'] if misses > 6
    map[6][1], map[6][3] = ['/', "\\"] if misses > 7

    map.map!{ |row| row.map! { |letter| letter.nil? ? ' ' : letter } }
    map.each { |row| row << "\n" }
    print map.join
  end
end

class HumanPlayer
  attr_reader :secret_word, :name, :dictionary, :guesses

  def initialize(name, dictionary)
    @name = name
    @dictionary = dictionary
  end

  def pick_secret_word
    puts 'Pick a word: '
    word = gets.chomp.downcase
    raise if !dictionary.include?(word)

    @secret_word = word
    @secret_word.length
  rescue
    puts 'Word not in dictionary'
    retry
  end

  def check_guess(letter)
    hits = []
    @secret_word.chars.each_index do |i|
      hits.push(i) if @secret_word[i] == letter
    end
    hits
  end

  def register_secret_length(length)
    puts "The word is #{length} letters long"
    @guesses = []
  end

  def guess(board)
    puts 'Pick a letter: '
    letter = gets.chomp
    raise 'Invalid input' if !('a'..'z').cover?(letter)
    raise 'Already guessed' if board.include?(letter)
    raise 'Already guessed' if guesses.include?(letter)

    @guesses.push(letter)
    letter

  rescue Exception => msg
    puts msg
    retry
  end

  def handle_response(letter, hits)
    if hits.length > 1
      puts "There are #{hits.length} #{letter}'s'!"
    elsif hits.length == 1
      puts "There is 1 #{letter}!"
    else
      puts "There are no #{letter}'s' :("
    end
  end
end

class ComputerPlayer
  attr_reader :dictionary, :secret_word

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    letters = @secret_word.chars
    letters.each_index.select { |i| secret_word[i] == letter }
  end

  def register_secret_length(length)
    @secret_word_length = length
    @dictionary.delete_if { |word| word.length != length }
  end

  def guess(board)
    letter_count = Hash.new(0)
    candidate_words.each do |word|
      word.chars.each { |letter| letter_count[letter] += 1 }
    end
    elligible_letters = letter_count.each_pair.select do |pair|
      !board.include?(pair[0])
    end
    elligible_letters.max_by { |pair| pair[1] }[0]
  end

  def candidate_words
    @dictionary
  end

  def handle_response(letter, hits)
    if !hits.empty?
      @dictionary.delete_if do |word|
        word.chars.count(letter) != hits.length ||
        hits.any? { |i| word[i] != letter }
      end
    else
      @dictionary.delete_if { |word| word.include?(letter) }
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  dictionary = File.readlines('lib/dictionary.txt')
  dictionary.map!(&:chomp)

  human = HumanPlayer.new('keith', dictionary)
  comp = ComputerPlayer.new(dictionary)

  game = Hangman.new(guesser: human, referee: comp)
  game.play

  puts "\n\nSWITCHING PLAYERS"
  dictionary = File.readlines('lib/dictionary.txt')
  dictionary.map!(&:chomp)

  human = HumanPlayer.new('keith', dictionary)
  comp = ComputerPlayer.new(dictionary)

  game = Hangman.new(referee: human, guesser: comp)
  game.play
end
